window.onload = function () {
    var width, height, area;
    var boxHeight = 98, boxWidth = 80;
    var boxArea = boxHeight * boxWidth;
    var actualImage = new Image();
    actualImage.src = $(".exercise-container0")
      .css("background-image")
      .replace(/"/g, "")
      .replace(/url\(|\)$/gi, "");

    width = actualImage.width;
    height = actualImage.height;
    console.log("Height : " + height, ";Width : " + width);
    var area = height * width;
    console.log("Total Area : " + area);
    var boxNeeded = area / boxArea;
    boxNeeded = parseInt(boxNeeded);
    console.log("Total Box Need : ", boxNeeded);

    var count = 162;
    var border = 1;
    var totalPic = 0;
    var top = -99;

    for (let index = 0; index < 20; index++) { 
      var div = document.createElement("div");
      div.id = "container";
      div.className = "exercise-box";

      div.style.backgroundPosition = "right " + count + " top 0";

      $(".exercise-container").append(div);
      console.log("Right : " + count + " Top : 0");

      count = count + 80 + border;
      totalPic++;
    } 
    console.log("Total Pic : " + totalPic);

    count = 80;
    for (let index = 0; index < 22; index++) {
      var div = document.createElement("div");
      div.id = "container";
      div.className = "exercise-box";

      div.style.backgroundPosition = "right " + count + " top " + top;

      $(".exercise-container").append(div);
      console.log("Right : " + count + " Top : " + top);

      count = count + 80 + border;
      totalPic++;

      if (index == 21) {
        count = 80;
        top = top - 99;
        index = 0;
      }
      if (totalPic == 314) {
        break;
      }
      console.log("Total Pic : " + totalPic);
    }
    console.log("Total Pic : " + totalPic);
  };