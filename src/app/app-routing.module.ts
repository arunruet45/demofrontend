import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImageComponent } from './components/image/image.component';
import { SelectedExerciseReportComponent } from './components/selected-exercise-report/selected-exercise-report.component';


const routes: Routes = [
  {path: 'filteredImages', component: ImageComponent},
  {path: 'selectedExerciseReport', component: SelectedExerciseReportComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
