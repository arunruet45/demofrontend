import { TestBed } from '@angular/core/testing';

import { FilteredImageShareService } from './filtered-image-share.service';

describe('FilteredImageShareService', () => {
  let service: FilteredImageShareService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FilteredImageShareService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
