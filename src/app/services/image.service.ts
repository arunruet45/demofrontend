import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  filterImageUrl = "http://localhost:4000/api/v1/persons";

  constructor(private http: HttpClient) { }

  getImages(filterProperties: any) {
    return this.http.get(this.filterImageUrl, {observe: 'response'});
  }
}
