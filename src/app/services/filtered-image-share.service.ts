import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilteredImageShareService {

  constructor() { }

  private ImageSource = new BehaviorSubject<Object>({});
  currentImages = this.ImageSource.asObservable();

  changeImages(images: any) {
    this.ImageSource.next(images);
  }
}
