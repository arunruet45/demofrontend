import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs'; 
import { debounceTime } from 'rxjs/operators';
import { ImageService } from 'src/app/services/image.service';
import { Router } from '@angular/router';
import { FilteredImageShareService } from 'src/app/services/filtered-image-share.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  searchText: string;
  condition: string;
  exerciseDifficulty: string[];
  equipmentAvailable: string[];
  exerciseType: string[];
  bodyPart: string[];
  ageCategory: string[];
  imageOrientation: string[];
  selectTextToDisplayWithExerciseImage: string[];

  haveCondition: boolean;
  haveExerciseDifficulty: boolean;
  haveEquipmentAvailable: boolean;
  haveExerciseType: boolean;
  haveBodyPart: boolean;
  haveAgeCategory: boolean;
  haveImageOrientation: boolean;
  haveSelectTextToDisplayWithExerciseImage: boolean;

  filterProperties: any;
  searchTextChanged: Subject<string> = new Subject<string>();

  constructor(private imageService: ImageService, private router: Router, private filteredImageShare: FilteredImageShareService) {
    this.searchText = null;
    this.condition = null;
    this.ageCategory = null;
    this.imageOrientation = null;
    this.haveCondition = false;
    this.haveExerciseDifficulty = false;
    this.haveEquipmentAvailable = false;
    this.haveExerciseType = false;
    this.haveBodyPart = false;
    this.haveAgeCategory = false;
    this.haveImageOrientation = false;
    this.haveSelectTextToDisplayWithExerciseImage = false;
    this.exerciseDifficulty = [];
    this.equipmentAvailable = [];
    this.exerciseType = [];
    this.bodyPart = [];
    this.selectTextToDisplayWithExerciseImage = [];
    this.searchTextChanged.pipe(
      debounceTime(500)).subscribe((text) => {
          this.searchText = text; 
          this.getFilteredImage();
      });
  }

  ngOnInit(): void {
    
  }
  
  getSearchText(text: string) {
    this.searchTextChanged.next(text);
  }

  getCondition($event: any) {
    this.condition = $event.target.value;
    this.haveCondition = true;  
    this.getFilteredImage();
  }

  getExerciseDifficulty($event: any) {
    if ($event.target.checked) {
      this.exerciseDifficulty.push($event.target.value);
    } else {
      this.exerciseDifficulty = this.exerciseDifficulty.filter(ed => ed != $event.target.value);
    }

    (this.exerciseDifficulty.length !== 0) ? this.haveExerciseDifficulty = true : this.haveExerciseDifficulty = false;
    
    this.getFilteredImage();
  }

  getEquipmentAvailable($event: any) {
    if ($event.target.checked) {
      this.equipmentAvailable.push($event.target.value);
    } else {
      this.equipmentAvailable = this.equipmentAvailable.filter(ea => ea != $event.target.value);
    }

    (this.equipmentAvailable.length !== 0 ) ? this.haveEquipmentAvailable = true : this.haveEquipmentAvailable = false;
  
    this.getFilteredImage();
  }

  getExerciseType($event: any) {
    if ($event.target.checked) {
      this.exerciseType.push($event.target.value);
    } else {
      this.exerciseType = this.exerciseType.filter(et => et != $event.target.value);
    }

    (this.exerciseType.length !== 0) ? this.haveExerciseType = true : this.haveExerciseType = false;
  
    this.getFilteredImage();
  }

  getBodyPart($event: any) {
    if ($event.target.checked) {
      this.bodyPart.push($event.target.value);
    } else {
      this.bodyPart = this.bodyPart.filter(bp => bp != $event.target.value);
    }

    (this.bodyPart.length !== 0) ? this.haveBodyPart = true : this.haveBodyPart = false;

    this.getFilteredImage();
  }

  getAgeCategory($event: any) {
    this.ageCategory = $event.target.value;
    this.haveAgeCategory = true;
    this.getFilteredImage();
  }

  getImageOrientation($event: any) {
    this.imageOrientation = $event.target.value;
    this.haveImageOrientation = true;
    this.getFilteredImage();
  }

  getSelectTextToDisplayWithExerciseImage($event: any) {
    if ($event.target.checked) {
      this.selectTextToDisplayWithExerciseImage.push($event.target.value);
    } else {
      this.selectTextToDisplayWithExerciseImage = this.selectTextToDisplayWithExerciseImage.filter(st => st != $event.target.value);
    }

    (this.selectTextToDisplayWithExerciseImage.length !== 0) ? this.haveSelectTextToDisplayWithExerciseImage = true : this.haveSelectTextToDisplayWithExerciseImage = false;
  
    this.getFilteredImage();
  }

  getFilterProperties() {
    this.filterProperties = {
      searchText: this.searchText,
      condition: this.condition,
      exerciseDifficulty: this.exerciseDifficulty,
      equipmentAvailable: this.equipmentAvailable,
      exerciseType: this.exerciseType,
      bodyPart: this.bodyPart,
      ageCategory: this.ageCategory,
      imageOrientation: this.imageOrientation,
      selectTextToDisplayWithExerciseImage: this.selectTextToDisplayWithExerciseImage
    }
    return this.filterProperties;
  }

  getFilteredImage() {
    this.filterProperties = this.getFilterProperties();
    console.log(this.filterProperties);
    this.imageService.getImages(this.filterProperties)
      .subscribe(
        (data) => {
          console.log(data);
          this.filteredImageShare.changeImages(data.body);
          this.router.navigate(['/filteredImages'])
        },
        (error) => {
          console.log(error);
        }
      );
  }

}
