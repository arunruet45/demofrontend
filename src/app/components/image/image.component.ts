import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FilteredImageShareService } from 'src/app/services/filtered-image-share.service';
import * as jsPdf from 'jspdf';
declare var $: any;

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {

  enableOutputOfSelectedExercises: boolean;
  enableImageOutputShow: boolean
  images: any[];
  selectedExerciseImages: any[];
  

  @ViewChild('content', {read: ElementRef}) private content: ElementRef;

  constructor(private filteredImageShare: FilteredImageShareService) { 
    this.enableOutputOfSelectedExercises = false;
    this.enableImageOutputShow = false;
    this.images = [];
    this.selectedExerciseImages = [];
  }

  ngOnInit(): void {
    // this.getFilteredImages(); 
  }

  getFilteredImages() {
    this.filteredImageShare.currentImages
      .subscribe(
        (images: any) => {
          console.log(images);
          this.images = images;
        }
      )
  }

  showSelectedImageOutput(image: any) {
    this.pushOrPopImageFromSelectedImage(image);
    this.selectedExerciseImages.length > 0 ? this.enableOutputOfSelectedExercises = true : 
      this.enableOutputOfSelectedExercises = false;
    this.enableImageOutputShow = true;
  }

  pushOrPopImageFromSelectedImage(image) {
    (this.selectedExerciseImages.findIndex(selectedImages => selectedImages === image)) === -1 ? 
      this.selectedExerciseImages.push(image) : this.selectedExerciseImages = this.selectedExerciseImages.filter(selectedImages => selectedImages !== image);
  }

  outputOfSelectedImageShowOrHide() {
    this.enableImageOutputShow ? this.enableImageOutputShow = false : this.enableImageOutputShow = true;
  }

  isImageSelected(image) {
    return ((this.selectedExerciseImages.findIndex(x => x === image)) === -1) ? false : true ;
  }

  downloadReportPdf() {
    let document = new jsPdf();
    let content = this.content.nativeElement;
    document.fromHTML(content.innerHTML, 15, 15);
    document.save("report.pdf");
  }
  
} 
