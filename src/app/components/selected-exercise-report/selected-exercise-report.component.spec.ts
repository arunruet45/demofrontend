import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedExerciseReportComponent } from './selected-exercise-report.component';

describe('SelectedExerciseReportComponent', () => {
  let component: SelectedExerciseReportComponent;
  let fixture: ComponentFixture<SelectedExerciseReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedExerciseReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedExerciseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
