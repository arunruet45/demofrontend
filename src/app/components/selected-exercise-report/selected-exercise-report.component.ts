import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-selected-exercise-report',
  templateUrl: './selected-exercise-report.component.html',
  styleUrls: ['./selected-exercise-report.component.css']
})
export class SelectedExerciseReportComponent implements OnInit {

  @Input() selectedExerciseImages: any;
  constructor() { }

  ngOnInit(): void {
  }

}
